#CA5 -Part 2 - README file

### Adição de testes unitários ao projeto desenvolvido no CA2 - Parte 2

Neste passo, tive que primeiramente adicionar testes ao projeto ca2-part2 pois apos o ter verificado constatei que nao tinha qualquer tipo de testes.
Foram adicionados assim, dois testes unitarios, um para ver se um objeto 'Employee' era *instanceOf* da classe Employee e outro para testar o metodo getLastName.

### Criação de Pipeline simplificada - Jenkins 

Tendo em conta que as stages *Checkout*, *Assemble*, *Test* e *Archive* eram iguais às que foram adicionadas no projeto do CA5-Part1, aproveitei o trabalho que ja tinha sido desenvolvido para essas mesmas stages, modificando apenas os diretorios para o qual este teria de apontar (neste caso CA2/part2_ca5_part2/demo) e só posteriormente é que adicionei as stages *Javadoc* e *Publish Image*.

Depois de algumas afinações em relação a script da ca5-part1, cheguei a este script que cumpre os requisitos destas quatro etapas.
 
    pipeline {
       agent any
       
               stages {
                   stage('Checkout') {
                       steps {
                           echo 'Checking out...'
                           git  'https://fuzionpt@bitbucket.org/diogorodriguesISEP/devops-19-20-b-1191757.git'
                       }
                   }
       
                   stage('Assemble') {
                       steps {
                           echo 'Building...'
                           bat 'cd CA2/part2_ca5_part2/demo & gradlew clean assemble'
                       }
                   }
       
                   stage ('Test'){
                       steps {
                           echo 'Testing...'
                           bat 'cd CA2/part2_ca5_part2/demo & gradlew test'
                           junit 'CA2/part2_ca5_part2/demo/build/test-results/test/*.xml'
                       }
                   }

Numa segunda fase, foram adicionadas as seguintes linhas para proceder à implementação da stage *Javadoc*:


       stage('Javadoc'){
                        steps {
                            echo 'Publishing...'
                            bat 'cd CA2/part2_ca5_part2/demo/ & gradlew javadoc'
                            
                            publishHTML([
                                reportName: 'Javadoc',
                                reportDir: 'CA2/part2_ca5_part2/demo/build/docs/javadoc/',
                                reportFiles: 'index.html',
                                keepAll: false,
                                alwaysLinkToLastBuild: false,
                                allowMissing: false
                            
                                ])
                        }
                    }
                    
        
Por fim, tentei implementar a etapa *Publish Image*.

       stage('Publish Image') {
                       steps {
                           echo 'Publishing image...'
                           script {
                               docker.withRegistry('https://index.docker.io/v1/', 'diogorodrigues-Docker'){
                                                       def customImage =
                                                       docker.build("1191757/devops-19-20-b-1191757:${env.BUILD_ID}", "CA5/part2")
                                                       customImage.push()
                               }
                           }
                       }
                   }
                   

### Criação de Pipeline - Correr o Jenkinsfile através do Repositório Bitbucket

Para cumprir esta etapa, foi necessario criar um novo item ao qual foi dado o nome (DevOps_Ca5_Part2_BitBucketVersion) e selecionar a opçao pipeline.

Depois na parte da "definition pipeline" foi selecionada a opçao 'Pipeline script from SCM', selecionou-se o Git e foi tambem necessario introduzir o URL do meu repositorio:
 
    https://fuzionpt@bitbucket.org/diogorodriguesISEP/devops-19-20-b-1191757.git 
    
No campo das credenciais sendo o meu repositorio publico, ficou a branco.

Os restantes campos ficaram com a opçao default à execçao do **Script path** onde foi necessario colocar o caminho do Jenkins file que seria executado.
      
     CA5/part2/Jenkinsfile
     
 Depois isso é necessário *Save/Apply* as alterações e ir à Pipeline e fazer o *Build Now*.
 
 
 ###Envio do Relatorio e marcar o repositorio com a tag:
 
         git commit -a -m "fix#21: CA5-part2 -READMEfile"
         git push
         
         git tag -a ca5-part2 -m "CA5-PART2"
         git push origin ca5-part2
 
#CA5 -Part 1 - README file

#### 1- The goal is to create a very simple pipeline (similar to the example from the lectures). Note that this project should already be present in a folder inside your individual repository! When configuring the pipeline, in the Script Path field, you should specify the relative path (inside your repository) for the Jenkinsfile. For instance, ’ca2/part1/gradle-basic-demo/Jenkinsfile’.

Para completar este passo foi primeiramente feito o download do ficheiro jenkins, acedendo ao seguinte link:


        https://www.jenkins.io/doc/book/installing/#war-file
        

De seguida, foi aberto um terminal da linha de comandos e mudou-se para o directório que continha o ficheiro que tinha sido downloaded no passo anterior e foi corrido o seguinte comando:

    java -jar jenkins.war
    
Estando o comando executado, aparece no terminal uma password que terá de ser utilizada para fazer o setup da conta Jenkins.


Ir a http://localhost:8080, utilizar a password referida anteriormente, para criar a conta no Jenkins.

Seleccionei  a opção de instalar os plugins sugeridos.

Criei a primeira conta de administrador com o utilizador 1191757@isep.ipp.pt e com o User ID - diogo rodrigues.


Para completar este passo foi primeiramente instalado o ficheiro jenkins.war pela linha de comandos, utilizando o seguinte comando:

    java -jar jenkins.war
    
Estando isto feito, foi criado o ficheiro 'Jenkinsfile' onde seriam posteriamente adicionadas as linhas do meu script.

#### 2- You should define the following stages in your pipeline: Checkout. To checkout the code form the repository Assemble. Compiles and Produces the archive files with the application. Do not use the build task of gradle (because it also executes the tests)! Test. Executes the Unit Tests and publish in Jenkins the Test results. See the junit step for further information on how to archive/publish test results. Do not forget to add some unit tests to the project (maybe you already have done it). Archive. Archives in Jenkins the archive files (generated during Assemble)

Acedendo a http://localhost:8080, foi necessario ir à opçao *New Item* para proceder à criação da nova pipeline ao qual foi dado o nome (devops-ca5), selecionar a opçao Pipeline e clicar em OK.
De seguida, acedi à pagina de configuração e escolhi a opção *Pipeline script*.

Estando adicionado o nosso script, basta clicar em *Save/Apply* e ir ao menu principal e clicar onde diz *Build Now*.

Na parte do script foram adicionadas as seguintes linhas:


    PipelineScript:
    
        pipeline {
            agent any
    
            stages {
                stage('Checkout') {
                    steps {
                        echo 'Checking out...'
                        git  'https://fuzionpt@bitbucket.org/diogorodriguesISEP/devops-19-20-b-1191757.git'
                    }
                }
    
                stage('Assemble') {
                    steps {
                        echo 'Building...'
                        bat 'cd CA2/part1.1/gradle_basic_demo & gradlew clean assemble'
                    }
                }
    
                stage ('Test'){
                    steps {
                        echo 'Testing...'
                        bat 'cd CA2/part1.1/gradle_basic_demo & gradlew test'
                        junit 'CA2/part1.1/gradle_basic_demo/build/test-results/test/*.xml'
                    }
                }
    
                stage('Archive') {
                    steps {
                        echo 'Archiving...'
                        archiveArtifacts 'CA2/part1.1/gradle_basic_demo/build/distributions/*'
                    }
                }
            }
        }

### Criação de Pipeline - Correr o Jenkinsfile através do Repositório 

Apos ter chegado a um script funcional, foi adicionado ao ficheiro anterioramente referido, 'Jenkinsfile' o mesmo script descrito no passo acima.


Para a criação desta pipeline é necessário ir a *New Item*, escolher um nome para a pipeline (DevOps_CA5_part1_BitbucketVersion), seleccionar *Pipeline* e clickar em OK.

É agora necessário editar a página de configuração do *Job*.

Aqui é necessário seleccionar *Pipeline script from SCM*.

No campo *SCM* seleccionar *Git*.

No campo *Repository URL* é necessário colocar o URL da nossa conta do Bitbucket, no meu caso:

    https://fuzionpt@bitbucket.org/diogorodriguesISEP/devops-19-20-b-1191757.git  

No campo *Credentials*, no meu caso, sendo o meu repositorio publico nao foi necessario colocar credenciais.

Depois no campo *Script Path* em que é necessário colocar o *path* para o ficheiro Jenkinsfile, foi adicionado o seguinte path:

     CA5/part1/Jenkinsfile
    
Depois isso é necessário *Save/Apply* as alterações e ir à Pipeline e fazer o *Build Now*.

Resultando em build bem sucedida

### Explicação do script do ficheiro Jenkinsfile para as diferentes *Stages*

####  Checkout Stage

As seguintes linhas servem para aceder ao código do repositório:

    stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git 'https://fuzionpt@bitbucket.org/diogorodriguesISEP/devops-19-20-b-1191757.git '
                }
            }


#### Assemble Stage

Para fazer a compilação da aplicação e produzir os ficheiros de arquivo:

      stage('Assemble') {
                steps {
                    echo 'Building...'
                    bat 'cd CA2/part1.1/gradle_basic_demo & gradlew clean assemble'
                }
            }

Aqui é feita a mudança de directório, para a pasta que contém o ficheiro de inicialização da aplicação e de seguida é feita a tarefa *clean* que apaga o *Project build directory* e de seguida é realizada a tarefa *assemble*.
 
#### Test Stage

Para executar os testes unitários e publicar os seus resultados, foi implementado o seguinte código:

     stage ('Test'){
                steps {
                    echo 'Testing...'
                    bat 'cd CA2/part1.1/gradle_basic_demo & gradlew test'
                    junit 'cd CA2/part1.1/gradle_basic_demo/build/test-results/test/*.xml'
                }
            }

Aqui também é necessário alterar o directório para a *root* do projeto *gradle_basic_demo* e executar a tarefa *test* para correr os testes unitários.

A linha de código do junit permite publicar os resultados dos testes unitários.

#### Archive Stage

Para arquivar os ficheiros gerados durante a *Stage Assemble* no Jenkins, foi implementado o seguinte código:

     stage('Archive') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'cd CA2/part1.1/gradle_basic_demo/build/distributions/*'
                }
            }

O step archiveArtifacts permite seleccionar quais os ficheiros que se pretende arquivar.


#### 3- At the end of the part 1 of this assignment mark your repository with the tag ca5-part1.

    git commit -a -m "fix#20: CA5-part1- versao final readme"
    git push
    git tag -a ca5-part1 -m "CA5-PART1"
    git push origin ca5-part1
### Exercício CA4

###1- You should produce a solution similar to the one of the part 2 of the previous CAbut now using Docker instead of Vagrant.
###2- You should use docker-compose to produce 2 services/containers: web: this container is used to run Tomcat and the spring application db: this container is used to execute the H2 server database

###Getting Started

O primeiro passo consistiu em criar uma pasta CA4 e enviar para o repositorio.

De seguida, foi feito o download do repositorio dado como exemplo pelo professor:

    https://bitbucket.org/atb/docker-compose-spring-tut-demo/
   
   
E foi colocado esse repositorio dentro da pasta que tinha sido criada (CA4).

De seguida foi alterado o ficheiro 'Dockerfile'. Relativamente às alteraçoes, foram as seguintes:

    RUN git clone https://fuzionpt@bitbucket.org/diogorodriguesISEP/devops-19-20-b-1191757.git  (para fazer referencia ao meu repositorio)
    WORKDIR /tmp/build/devops-19-20-b-1191757/CA2/part2_v2_ca3part2/demo                        (para redicionar para a pasta que iria ser executada)
    RUN chmod +x gradlew                                                                        (para dar permissoes de executaçao ao gradlew)
    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/                        (alterado de basic para demo)
    

Estando todas as alteraçoes concluidas, e tendo o Docker tool-box instalado no computador, foi executado dentro da pasta que contem o ficheiro 'docker-compose.yml':

    'docker-compose build'

Resultando em 'Build Successful', foi executado o docker atraves do seguinte comando:

    `docker-compose up`


Estando o comando anterior executado e tudo a correr como é suposto, foi acedido via browser ao seguinte URL:

    http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/
    

Também foi possivel aceder à H2 console utilizado o URL:    
    
    http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/h2-console



###3- Publish the images (db and web) to Docker Hub (https://hub.docker.com)

    1. Log in on https://hub.docker.com/
    2. Clicar em  Create Repository.
    3. Escolher um nome (e.g. devops-19-20-b-1191757) e uma descriçao para o repositorio e clicar em Create.
    4. Fazer log in no DockerHub a partir da linha de comandos,e inserir as credencias necessarias.
    `docker login`
    5. Verificar o imageID utilizando o comando: 
    `docker images`
    6. Fazer Tag à image que queremos fazer push atraves do comando (ca4_db): 
    `docker tag 457f6841f901 1191757/devops-19-20-b-1191757:db`
    7. Fazer push da image para o repositorio criado utilizando o comando: 
    `docker push 1191757/devops-19-20-b-1191757:db`
    8. Fazer tag à image que queremos fazer push atraves do comando (ca4_web):
    'docker tag 013598447b5b 1191757/devops-19-20-b-1191757:web'
    9. Fazer push da image para o repositorio criado utilizando o comando:
    'docker push 1191757/devops-19-20-b-1191757:web'
    


###4- Use a volume with the db container to get a copy of the database file by using the exec to run a shell in the container and copying the database file to the volume.

Para completar este passo foram executados os seguintes comandos:

    docker-compose up
    
    E depois, noutra janela de cmd, foi executado:
    docker-compose exec db bash
    cp /usr/src/app/jpadb.mv.db /usr/src/data   (comando para copiar o ficheiro de database)


###6- At the end of this assignment mark your repository with the tag ca4.

    git commit -a -m "fix ##19: CA4 - Docker- final version for readme"
    git push
    git tag ca4
    git push origin ca4
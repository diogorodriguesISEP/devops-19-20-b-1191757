### Exercício CA3-Part 2

###1 You should use https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ as an initial solution.

Foi primeiramente criada a pasta CA3 -> part2 e o respetivo ficheiro README e enviado para o repositorio. 


    git commit -m -a "ref #18: CA3-part2- creating folder and readmefile"
    git push 

Depois foi feito o download do repositorio indicado e seguindo os passos do ficheiro README desse repositorio, foi executado o comando:

    vagrant up
        
Verificando-se no VirtualBox a criaçao de duas novas VM's (uma com o nome part2_db... e outra part2_web..) e estando elas a correr, acedeu-se aos seguinte links:

    http://localhost:8080/basic-0.0.1-SNAPSHOT/
    http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/
    http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
    http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console
    



###3 Copy this Vagrantfile to your repository (inside the folder for this assignment).

Para completar este passo foi necessario copiar o ficheiro 'Vagrantfile' contido no download do repositorio indicado no passo 1 e colocado dentro da pasta para este exercicio (CA3-part2).



###4 Update the Vagrantfile configuration so that it uses your own gradle version of the spring application.

Para completar este passo foi primeiramente necessário colocar o repositorio publico devido a um problema de autenticaçao, onde nao era possivel aceder ao repositorio pois nao tinha maneira de colocar a password do mesmo.
De seguida, na parte final do ficheiro 'Vagrantfile', foram feitas as seguinte alteraçoes:

      # Change the following command to clone your own repository!
          git clone https://fuzionpt@bitbucket.org/diogorodriguesISEP/devops-19-20-b-1191757.git
          cd devops-19-20-b-1191757/CA2/part2_v2_ca3part2/demo
          chmod +x gradlew
          command:
                sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

###5 Check https://bitbucket.org/atb/tut-basic-gradle to see the changes necessary so that the spring application uses the H2 server in the db VM. Replicate the changes in your own version of the spring application.

Foi inicialmente criada uma copia da part2 do CA2 (part2_v2_ca3part2) dentro da pasta CA2 onde seriam replicadas todas as alteraçoes necessárias.


#### 1st Commit - ADD: Support for building war file.

No ficheiro build.gradle foram adicionadas as seguintes linhas:

        id 'war'
        // To support war file for deploying to tomcat
        providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
        
No caminho src/main/java/com/greglturnquist/payroll/ foi criada a classe  **ServletInitializer.java**  com as seguintes linhas de codigo:

    package com.greglturnquist.payroll;
    
    import org.springframework.boot.builder.SpringApplicationBuilder;
    import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
    
    public class ServletInitializer extends SpringBootServletInitializer {
    
        @Override
        protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
            return application.sources(ReactAndSpringDataRestApplication.class);
        }
    }
    
Estando estas alteraçoes concluidas, foi feito commit para o repositorio.
    
    
#### 2nd Commit - ADD: Support for h2 console.

No caminho  src/main/resources/  no ficheiro  **application.properties** foram adicionadas as seguintes linhas:

    spring.datasource.url=jdbc:h2:mem:jpadb
    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=sa
    spring.datasource.password=
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    spring.h2.console.enabled=true
    spring.h2.console.path=/h2-console
    
Estando estas alteraçoes concluidas, foi feito commit para o repositorio.


### 3rd Commit - ADD: weballowothers to h2.

No caminho  src/main/resources/  no ficheiro  **application.properties** foi adicionada a seguinte linha:

    spring.h2.console.settings.web-allow-others=true

Estando esta alteraçao concluida, foi feito commit para o repositorio.


### 4th Commit - ADD: Application context path.

No caminho src/main/js/  no ficheiro **app.js** foi atualizada a seguinte linha:

    client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {

No caminho  src/main/resources/  no ficheiro  **application.properties** foi adicionada a seguinte linha:

    server.servlet.context-path=/demo-0.0.1-SNAPSHOT
    
Estando estas alteraçoes concluidas, foi feito commit para o repositorio.


### 5th Commit - UPDATE: Fixes ref to css in index.html

No caminho src/main/resources/templates/ no ficheiro  **index.html**  foi atualizada a seguinte linha:

     <link rel="stylesheet" href="main.css" />
     
Estando esta alteraçao concluida, foi feito commit para o repositorio.


### 6th Commit - ADD: Settings for remote h2 database.

No caminho  src/main/resources/  no ficheiro  **application.properties** foram comentadas e adicionadas as seguintes linhas ,respetivamente:

    #spring.datasource.url=jdbc:h2:mem:jpadb
    spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/~/jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    
Estando estas alteraçoes concluidas, foi feito commit para o repositorio.
    
    
### 7th Commit - UPDATE: Fixes h2 connection string.

No caminho  src/main/resources/  no ficheiro  **application.properties** foram adicionadas as seguintes linhas:

    # In the following settings the h2 file is created in /home/vagrant folder
    spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE

Estando estas alteraçoes concluidas, foi feito commit para o repositorio.


### 8th Commit - UPDATE: So that spring will no drop de database on every execution

No caminho  src/main/resources/  no ficheiro  **application.properties** foram adicionadas as seguintes linhas:

    # So that spring will no drop de database on every execution.
    spring.jpa.hibernate.ddl-auto=update

Estando estas alteraçoes concluidas, foi feito commit para o repositorio.


Para verificar se tudo estava a correr como era suposto foi aberto o terminal, mudou-se para o caminho que continha o ficheiro Vagrantfile (neste caso, CA3/part2) e foi executado o comando **vagrant up**  para serem inicializadas as VMs de db e web.
 
 Estando as VM's inicializadas acedeu-se aos seguintes links:

      http://localhost:8080/demo-0.0.1-SNAPSHOT/
      http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/
      http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
      http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console 
      
E verificou-se que estava tudo funcional. 




###6 Describe the process in the readme file for this assignment.
###7 At the end of the part 2 of this assignment mark your repository with the tag ca3-part2.


        git commit -a -m "fix #18- Versao final readme v2"
        git push
        git tag ca3-part2
        git push origin ca3-part2




### Exercício CA3-Part 1


#### 1 - You should start by creating your VM as described in the lecture
#### Getting Started

O primeiro passo consistiu em criar uma nova pasta CA3 e dentro dessa pasta uma sub-pasta chamada part1 e envia-las para o repositorio utilizando os seguintes comandos:


```
git add CA3

git commit -m "ref #17 CA3- part1"

git push
```

Depois disso, seguindo os passos da aula:

      
        1- Foi feito o download do ISO para ser mais tarde utilizado na instalaçao da VM.
        2- Foi criada uma nova VM no VirtualBox.
        2- No menu principal do VirtualBox, ir a File -> Host Network Manager, criar um novo host-only network com o seguinte IP: 192.168.56.1.
        3- Nos settings na nova VM -> Network, definir o Adapter1 como NAT e o Adapter2 definir como 'Host-only-adapter' e selecionar o host criado no passo acima.
        4- Ir novamente aos settings da nova VM -> Storage -> Controller:IDE e selecionar o disco 'mini.iso' (passo 1).
        5- Iniciar a VM e instalar o 'mini.iso'.
        6- Estando a instalação concluida, foi removido o disco, iniciou-se a VM e foi feito o log-in.
        
De seguida, foram executados os seguintes comandos:

        1- sudo apt install net-tools
        2- sudo nano /etc/netplan/01-netcfg.yaml    (Editar a configuraçao da network para ficar igual ao que foi feito na aula)
        3- Apos o comando anterior ser executado somos direcionados para um ficheiro editavel e foram escritas as seguintes settings:
        
        network:
        version: 2
        renderer: network
         ethernets:
            enp0s3:
            dhcp4: yes
            enp0s8:
            addresses:
                - 192.168.56.5/24
                
         4- sudo netplan apply
         5- sudo apt install openssh-server (Permite utilizar o SSH para abrir sessoes de terminal seguras)
         6- sudo apt install vsftpd
         7- sudo nano /etc/vsftpd.conf (Para editar o ficheiro de vsftpd)
         7.1 - Descomentar a frase 'write_enable=YES`
         7.2 - Correr o comando sudo service vsftpd restart 
         
         
No proximo passo, foi aberto o terminal do computador e correu-se o seguinte comando: 

```
ssh diogorodrigues@192.168.56.5
```

Para instalar o Git na VM, utilizou-se o comando:
```
sudo apt install git
```

Para instalar o Java, utilizou-se o comando:

```
sudo apt install openjdk-8-jdk-headless
```
                

#### 2 - You should clone your individual repository inside the VM

Para concluir este passo foi executado o seguinte comando no terminal:

    git clone https://fuzionpt@bitbucket.org/diogorodriguesISEP/devops-19-20-b-1191757.git


#### 3 - You should try to build and execute the spring boot tutorial basic project and the gradle_basic_demo project (from the previous assignments)
##### You should report and explain possible issues you may encounter in your readme file!

#### 4 -  For web projects you should access the web applications from the browser in your host machine (i.e., the "real" machine)

##Run CA1
3.1 -Foi corrido o seguinte comando dentro da pasta do projeto na VM para instalar o maven:
    
    sudo apt-get install maven
    chmod +x mvnw		(para dar permissoes de execuçao)
    
Depois foi corrido o comando, no pasta CA1/basic:

    ./mvnw spring-boot:run  
    
Resultando em "Build Sucess" acedeu-se via browser http://192.168.56.5:8080/ para confirmar que estava a funcionar.


##Run CA2-part1
3.2- Para instalar o gradle na VM foi corrido o seguinte comando:

    sudo apt install gradle
    chmod +x gradlew		(para dar permissoes de execuçao)
    
Estando o gradle instalado, mudou-se para o diretorio CA2-> part1.1-> gradle_basic_demo e foi executado o comando para dar build ao projeto:

	.gradlew build   (resultando em "Build Sucess")



##Run CA2-part2
3.3-Foram corridos os seguintes comandos no diretorio CA2-> part2-> demo:

	chmod +x gradlew	(dar permissoes de execuçao)
	./gradlew build

Resultando em erro - "Build failed" - Task:assembleFrontend FAILED- build failed with an exception.

Para resolver o problema foram executados os comandos:
	
	sudo rm -Rf node
	sudo rm -Rf node_modules
	./gradlew clean build

Resultando em "Build successful!" 

De seguida, para correr a aplicaçao, o executado o comando:

	./gradlew bootRun 

Não tendo dado nenhum erro, foi possivel abrir a aplicaçao em:

	
    http://192.168.56.5:8080  
  


#### 6 - Describe the process in the readme file for this assignment.
#### 7 -  At the end of the part 1 of this assignment mark your repository with the tag ca3-part1.

        git commit -a -m "fix #17- Versao final readme ca3-part1"
        git push
        git tag ca3-part1
        git push origin ca3-part1


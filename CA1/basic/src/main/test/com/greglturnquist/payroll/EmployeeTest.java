package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void constructorTest () {
        Employee e1 = new Employee("diogo", "rodrigues", "isep", "student", "1191757@isep.ipp.pt");

        assertTrue (e1 instanceof Employee);
    }

    @Test
    void constructorTestFirstNameCantBeNull () {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee e1 = new Employee(null, "rodrigues", "isep", "student", "1191757@isep.ipp.pt");
        });

    }

    @Test
    void constructorTestFirstNameCantBeEmpty () {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee e1 = new Employee("", "rodrigues", "isep", "student", "1191757@isep.ipp.pt");
        });

    }

    @Test
    void constructorTestLastNameCantBeNull () {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee e1 = new Employee("diogo", null, "isep", "student", "1191757@isep.ipp.pt");
        });

    }

    @Test
    void constructorTestLastNameCantBeEmpty () {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee e1 = new Employee("", "", "isep", "student", "1191757@isep.ipp.pt");
        });

    }

    @Test
    void constructorTestDescriptionCantBeNull () {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee e1 = new Employee("diogo", "rodrigues", null, "student", "1191757@isep.ipp.pt");
        });

    }

    @Test
    void constructorTestDescriptionCantBeEmpty () {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee e1 = new Employee("", "rodrigues", "", "student", "1191757@isep.ipp.pt");
        });

    }


    @Test
    void constructorTestEmailCantBeNull () {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee e1 = new Employee("diogo", "rodrigues", null, "student", null);
        });

    }

    @Test
    void constructorTestEmailCantBeEmpty () {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee e1 = new Employee("", "rodrigues", "", "student", "");
        });

    }

    @Test
    @DisplayName ("Test to validate that email needs to have @ character")
    void contructorTestEmailNeedsToHaveAtSign () {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee e1 = new Employee("", "rodrigues", "", "student", "1191757isep.ipp.pt");
        });
    }



}
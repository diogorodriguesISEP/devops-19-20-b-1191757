#**1. Analysis, Design and Implementation**

**1 .Tag the initial version as v1.2.0:**

Devem ser utilizados os comandos:

    git tag v1.2.0              (criar tag na versao inicial)
    git push origin v1.2.0      (push da tag para o repositório)


**2. You should create a branch called email-field:**

Deve ser utilizado o comando:
    
    git checkout -b email-field     (cria o novo branch e muda para esse branch)

**3. You should add support for email field:**

Foram feitas algumas alteraçoes ao repositório inicial para concluir este passo, primeiramente, foi adicionado à classe Employee e
ao seu respetivo constructor um novo atributo Email(type String). De seguida, na base de dados, mais precisamente em _DatabaseLoader.java_
foi incluido o novo campo email. Por ultimo, no ficheiro *app.j*s adicionou-se as seguintes linhas:
   
    <th>Email</th>
    <td>{this.props.employee.email}</td>


**4- You should also add unit tests for testing the creation of Employees and the validation of its attributes 
(for instance, no null/empty values):**

Para concluir este passo foi necessário na classe _Employee_ , nos métodos Set colocar restrições para garantir que o atributo recebido 
como input não pode ser vazio (null) ou estar vazio (empty).

De seguida, foi criada uma classe _EmployeeTest_ onde realizei uma serie de testes ao constructor de Employee. 
É importante referir que da maneira como fiz, no meu constructor são chamados os metodos Set, sendo estes os responsaveis por fazer a validação 
do string input. Isto é, validar se os atributos inseridos não sao nulos nem vazios, à excepcão do atributo email em que é feita a confirmaçao
de que contém um "@", caso não se cumpram estes requisitos (não ser nulo, vazio ou contiver @, no caso do atributo email, são
sempre lançadas excepções.)



**5- You should debug the server and client parts of the solution:**

De forma a verificar se a aplicação continua funcional depois das referidas alterações, foi corrido o comando mvnw spring-boot_run 
no IDE, sendo possível constatar que a tabela de employees é carregada na página localhost:8080.
    
    1- mvnw spring-boot:run
    



**6- When the new feature is completed (and tested) the code should be merged with the master and a new 
tag should be created (e.g, v1.3.0).**

Para completar este passo, foram executados estes comandos, pela seguinte ordem:

    1.	git add -A                 (comando para adicionar todos os ficheiros modificados)
    2.	git commit -a -m “fix #2”  (comando para fazer commit)
    3.	git push                   (comando para fazer push)
    4.	git checkout master        (comando para mudar para o branch master)
    5.	git merge email-field      (comando para fazer merge do branch email-field ao master)
    6.	git tag v1.3.0             (comando para fazer tag)
    7.      git push origin v1.3.0     (comando para fazer push da tag criada)
    




**7- Create a branch called fix-invalid-email. The server should only accept Employees with a valid email 
(e.g., an email must have the "@" sign):**

Utilizar o comando:
    
    git checkout -b fix-invalid-email   (Criar novo branch e mudar para esse branch)

Na classe _Employee_,  adicionar ao método _setEmail_ condiçoes que verifiquem que a string input contem o caracter @ e que não é null; 

    public void setEmail(String email) {

    if (email.contains("@") && email != null && !email.isEmpty()) {

        this.email = email;

    } else {

        throw new IllegalArgumentException ("Input email is invalid");

    }

}

**8- You should debug the server and client parts of the solution.**

De forma a verificar se a aplicação continua funcional depois das referidas alterações, foi corrido o comando mvnw spring-boot_run 
no IDE, sendo possível constatar que a tabela de employees é carregada na página localhost:8080.
    
    1- mvnw spring-boot:run



**9- When the fix is completed (and tested) the code should be merged into master and a new tag should be created 
(with a change in the minor number, e.g., v1.3.0 -> v1.3.1)**

Neste passo deparei-me com um problema no meu projeto, ao abrir o projeto, reparei que tinha perdido todas as alteraçoes que tinha feito até então
(nao sei ao certo o quê que se passou), no entanto já tinha uma tag v1.3.1 no repositorio e como não a consegui apagar, 
acusando que já se encontrava no repositorio, vi-me obrigado a refazer todos os passos e criar um nova tag(v1.3.2) que 
substituiria a pretendida. Dito isto, para completar este passo, foram executados estes comandos, pela seguinte ordem:

    1.	git add -A                      (comando para adicionar todos os ficheiros modificados)
    2.	git commit -a -m “fix #9”;      (comando para fazer commit)
    3.	git push                        (comando para fazer push)
    4.	git checkout -b master;         (comando para mudar para o branch master)
    5.	git merge fix-invalid-email;    (comando para fazer merge do branch fix-invalid-email ao master)
    6.	git tag v1.3.2;                 (comando para fazer tag)
    7.      git push origin v1.3.2          (comando para fazer push da tag criada)


**10- At the end of the assignment mark your repository with the tag ca1.**

Tal como referi no passo anterior, tendo perdido todas as alteraçoes tive de criar uma nova tag ca2 que substituirá a 
tag ca1, pois já se encontrava no repositorio e não consegui apaga-la.

Executar os seguintes comandos:

    1- git tag ca2.
    2- git push origin ca2.

#**2. Analysis of an Alternative**

Com base na pesquisa que realizei penso que o Mercurial poderá ser uma boa alternativa ao Git.

#####MERCURIAL

O Mercurial é um sistema de Controlo de Versões distribuído assim como o Git. Foi desenvolvido para suportar projetos de grande porte e
caracteriza-se por ser extremamente rápido, escalável e muito mais simples que o Git, uma vez que apresenta um leque muito menos extenso
de comandos disponiveis. Pode ainda ser considerado um sistema de Controlo de Versões bastante dedutivo e fácil de se aprender 
caso já se tenha algum conhecimento previo em CV pois parte do seus comandos vão de acordo com aqueles que existem por exemplo 
no Git. Para além disso, o Mercurial vem equipado com uma interface web e tem uma excelente documentação.


#####Git VS Mercurial - Diferenças

No que a diferenças diz respeito, após alguma pesquisa na internet pude concluir que no geral:

Numa perspetiva de utilização, quando comparamos o Git com Mercurial uma das grandes diferenças passa pelo nivel de 
conhecimento necessário para conseguir utilizar cada um destes sistemas.
O git é mais complexo e exige da parte de quem o utiliza um maior conhecimento de forma a conseguir utilizar este sistema
de CV de forma mais segura e eficiente. Para além disso, no que toca à documentação, esta pode ser visto por muitos como 
mais complicada de entender. Sendo que, caso estejamos a trabalhar num projeto com uma equipa com menos experiencia, 
provavelmente utilizar Mercurial será a melhor opção.

Numa perspetiva de segurança, penso que não posso afirmar que um é mais seguro que o outro. Pelo que consegui filtrar da minha pesquisa,
nenhum dos sistemas oferece um sistema de segurança que consiga impedir que os programadores compromentam codigo, quer 
acidentalmente quer intecionalmente. Ou seja, no geral, nenhum destes sistemas satisfaz a necessidade de segurança desejada.

No que diz respeito a Branching, ou seja, trabalhar em diferentes versoes de codigo ao mesmo tempo, na opiniao dos utilizadores
de Git, o Modelo de Branching do Git é mais eficiente que o do Mercurial, uma vez que cada branch faz referencia a um certo commit.
O Git permite que sejam criados, apagado e modificados branchs a qualquer altura sem que os commits sejam afetados.
Caso tenhamos necessidade de testar uma nova funcionalidade ou procurar um bug - apenas temos de criar um branch, 
efetuar as modificaçoes e depois apagar o branch. 

Em contrapartida, o Modelo de Branch do Mercurial pode gerar alguma confusão. Em Mercurial, os branches fazem referencia
a uma linha consecutiva de _changesets_ (Complete set of changes made to a file in a repository). Em comparaçao com o Git que
permite que haja uma maior flexibilidade no que diz respeito aos branches, em Mercurial nao existe esse tipo de flexibilidade.
Quando utilizamos Mercurial todos os branchs sao embutidos em commits, onde são guardados para sempre. Isto faz com que
não seja possível remover branchs pois causaria uma alteraçao no historico de commits. No entanto é possivel fazer referencia
a um certo commit atraves de _bookmarks_.

Por fim, o Mercurial exige que tenhamos atençao adicional para nao dar push a codigo para um branch errado, especialmente
caso nao tenhamos ainda atribuido um nome a esse branch.

Em suma, posso concluir que o Git e Mercurial são bastante parecidos, sendo que cada um deles traz as suas vantagens tal como 
desvantagens. São sistemas de Controlo de Versões que foram desenvolvidos de forma diferente e que requerem diferentes
niveis de conhecimento. O Git é geralmente a escolha de eleição entre developers e também mais adequado para equipas maiores, 
no entanto, caso trabalhemos em equipas mais pequenas e com um menor nivel de conhecimento, o Mercurial poderá ser uma boa opção
uma vez que tem como grande vantagem ser bastante facil de aprender e utilizar. 



##Referencias

https://www.atlassian.com/blog/git/git-vs-mercurial-why-git

https://stackoverflow.com/questions/35837/what-is-the-difference-between-mercurial-and-git

https://content.intland.com/blog/sdlc/why-is-git-better-than-mercurial

https://www.perforce.com/blog/vcs/git-vs-mercurial-how-are-they-different







 
    







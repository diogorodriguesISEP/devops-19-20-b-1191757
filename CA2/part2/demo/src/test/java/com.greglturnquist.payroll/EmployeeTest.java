package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class EmployeeTest {

    @Test
    void EmployeeConstructorHappyCaseTest(){

        Employee e1 = new Employee("diogo", "rodrigues", "isep student", "student");

        assertTrue (e1 instanceof Employee);
    }


    @Test
    void getLastNameHappyCaseTest (){

        Employee e1 = new Employee("diogo", "rodrigues", "isep student", "student");

        String expected = "rodrigues";
        String result = e1.getLastName();

        assertEquals(expected,result);

    }


}
package basic_demo;

import junit.framework.TestCase;
import org.junit.Test;

public class AppTest extends TestCase {

    @Test
    public void testAppHasAGreeting () {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());;

    }

}